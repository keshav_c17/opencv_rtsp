#include<opencv2/opencv.hpp>
#include<iostream>

int main() 
{
    cv::VideoCapture vid_capture("http://88.131.30.164/mjpg/video.mjpg");

    if (vid_capture.isOpened() == false) {
        std::cout << "Error opening video stream or file" << std::endl;
    }
    else{
        int fps = vid_capture.get(CV_CAP_PROP_FPS);
        std::cout << "FPS: " << fps << std::endl;

        int frame_count = vid_capture.get(CV_CAP_PROP_FRAME_COUNT);
        std::cout <<"Frame count: " << frame_count << std::endl;
    }

    while (vid_capture.isOpened()) {
        
        cv::Mat frame;

        bool isSuccess = vid_capture.read(frame);

        if (isSuccess == true){
            cv::imshow("Frame", frame);
        }
        else{
            std::cout << "Not receiving anymore frames.." << std::endl;
            break;
        }
        
        int key = cv::waitKey(20);
        if (key == 'q'){
            std::cout << "Stopping the stream as requested.." << std::endl;
            break;
        }

    }

    vid_capture.release();
    cv::destroyAllWindows();
    return 0;
}