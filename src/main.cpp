#include<opencv2/opencv.hpp>
#include<iostream>
#include <signal.h>

volatile sig_atomic_t stop;
struct timespec begin, end; 
double total_elapsed;
double avg_elapsed;


void inthand(int signum) {
    stop = 1;
}

void start_time(){
	// Start measuring time
    clock_gettime(CLOCK_REALTIME, &begin);
}

void end_time(bool error=false)
{
	// Stop measuring time and calculate the elapsed time
    clock_gettime(CLOCK_REALTIME, &end);
	double elapsed;
    long seconds;
	long nanoseconds;

	seconds = end.tv_sec - begin.tv_sec;
	nanoseconds = end.tv_nsec - begin.tv_nsec;
	
	// if (error) {
	// 	elapsed = 0;
	// 	total_elapsed += 0;
	// 	printf(", Error occured.. Time measured: %.3f ms.\n", elapsed);
	// }
	// else {
    elapsed = seconds + nanoseconds*1e-9;
    total_elapsed += elapsed*1000;
    printf(", Time measured: %.3f ms.\n", elapsed*1000);
	
}	



int main() 
{   
    int fps;
    signal(SIGINT, inthand);

    cv::VideoCapture vid_capture("/home/keshav/Videos/30mins.mp4");

    if (vid_capture.isOpened() == false) {
        std::cout << "Error opening video stream or file" << std::endl;
    }
    else{
        fps = vid_capture.get(CV_CAP_PROP_FPS);
        std::cout << "FPS: " << fps << std::endl;

        int frame_count = vid_capture.get(CV_CAP_PROP_FRAME_COUNT);
        std::cout <<"Frame count: " << frame_count << std::endl;
    }

    int frame_width = static_cast<int> (vid_capture.get(CV_CAP_PROP_FRAME_WIDTH));
    int frame_height = static_cast<int> (vid_capture.get(CV_CAP_PROP_FRAME_HEIGHT));
    cv::Size frame_size(frame_width, frame_height);


    cv::VideoWriter output_stream("output.mp4", 
                                  cv::VideoWriter::fourcc('H', '2', '6', '4'), 
                                  fps, frame_size, true);


    int count = 0;
    stop = 0;
    while (!stop) {
        
        start_time();

        cv::Mat frame;

        bool isSuccess = vid_capture.read(frame);

        if (isSuccess == true){
            output_stream.write(frame);
            std::cout << "Writing frame " << count;
            count += 1;
            cv::imshow("Live Streaming", frame);
        }
        else{
            std::cout << "Not receiving frames anymore.. Ending the stream." << std::endl;
            break;
        }
        
        int key = cv::waitKey(20);
        if (key == 'q'){
            end_time();
            std::cout << "Stopping the stream as requested.." << std::endl;
            break;
        }

        end_time();
        
    }

    printf("\nAverage Frame Write Time: %.3f ms.\n", total_elapsed/count);

    vid_capture.release();
    output_stream.release();
    cv::destroyAllWindows();
    return 0;
}